#!/bin/bash

#./LTLf2Inputs/ltlf2fol/ltlf2fol Benchmarks/BigProbs/bigprob_0.ltl NNF > Benchmarks/BigProbs/bigprob_0.mona
#sed '/^#/ d' Benchmarks/BigProbs/bigprob_0.mona > Benchmarks/BigProbs/bigprob_0.mona2
#mona -u -xw Benchmarks/BigProbs/bigprob_0.mona2 > Benchmarks/BigProbs/bigprob_0.dfa
#./Syft/bdd Benchmarks/BigProbs/bigprob_0.dfa Benchmarks/BigProbs/bigprob_0.part 1
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

MONA_BIN="../mona/bin/mona"
SYFT_BIN="./Syft/bdd"
INPUT_BENCHMARKS_DIR="../../"
OUTPUT_TEMP_FILES="exp_files"
mkdir ${OUTPUT_TEMP_FILES}

for l in `seq 1 6`;
do
# python bigprobs_generator.py $l
mkdir ${OUTPUT_TEMP_FILES}/L$l
for i in `seq 0 99`;
do
echo -n Length $l 
echo -n -e ' \t 'Experiment $i:
START=$(date +%s)
./ltlfkit/LTLf2FOL/ltlf2fol/ltlf2fol ${INPUT_BENCHMARKS_DIR}/L$l/bigprob_$i.ltl BNF > ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona
END=$(date +%s)
DIFF=$(( $END - $START ))
echo -n -e ' \t '$DIFF
sed '/^#/ d' ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona > ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona2
mv ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona2 ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona
START=$(date +%s)
ulimit -Sv 4000000     # Set ~4000 MB limit
timeout 1800s ${MONA_BIN} -u -xw ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.mona > ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.dfa
END=$(date +%s)
DIFF=$(( $END - $START ))
echo -n -e ' \t '$DIFF
START=$(date +%s)
ulimit -Sv 4000000     # Set ~4000 MB limit
SYFT=`timeout 1800s ${SYFT_BIN} ${OUTPUT_TEMP_FILES}/L$l/bigprob_$i.dfa ${INPUT_BENCHMARKS_DIR}/L$l/bigprob_$i.part 1`
END=$(date +%s)
DIFF=$(( $END - $START ))
echo -n -e ' \t '$DIFF' \t '

# echo $SYFT
# ./Syft/bdd Benchmarks/BigProbs/bigprob_$i.dfa Benchmarks/BigProbs/bigprob_$i.part 1
if [ "$SYFT" = "realizable" ];
then
    echo "${green}realizable${reset}"
elif [ "$SYFT" = "unrealizable" ]; then
    echo "${red}unrealizable${reset}"
else
    echo $SYFT
fi

done
done
