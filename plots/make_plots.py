import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec

#sns.set_palette("Set1", desat=.6)
sns.set_palette('muted')
# sns.set_style("white", {'grid.color': '.95'})
sns.set_style("ticks")
# style : dict, None, or one of {darkgrid, whitegrid, dark, white, ticks}


# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
pd.set_option('display.mpl_style', 'default') # Make the graphs a bit prettier
# figsize(15, 5)
figsize = (4,3)

plt.rc('font', family='serif', serif='Times')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=16)
plt.rc('ytick', labelsize=16)
plt.rc('axes', labelsize=18)

import ast

markers = [ '+' , ',' , '.' , '1' , '2' , '3' , '4' ]


def success_rates_by_length(df):
    import ast
    success_vec = []
    for length in range(1,7):
        length_df = df.loc[df['length'] == "Length %s " % length]
        realizable_df = length_df.loc[length_df['status'] == " realizable"]
        unrealizable_df = length_df.loc[length_df['status'] == " unrealizable"]
        # print(unrealizable_df.to_string())
        # exit()
        n_success = len( realizable_df ) + len( unrealizable_df)
        success_vec.append((length,n_success))
    
    
    fig, ax = plt.subplots(figsize=figsize)
    # plt.plot(success_vec)
    plt.scatter(*zip(*success_vec))
    ax.set_xlim(1,6)
    ax.set_ylim(0,100)
    # plt.set_aspect('equal')
    # plt.plot([0, 1], [0, 1], transform=plt.transAxes)
    plt.xlabel('number of subproblems')
    plt.ylabel('solved problems')
    
    # lims = [
    # np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    # np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    # ]
    
    # now plot both limits against eachother
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    from scipy.interpolate import spline
    
    xnew = np.linspace(1,6,300) #300 represents number of points to make between T.min and T.max
    data = zip(*success_vec)
    power_smooth = spline(data[0],data[1],xnew)
    plt.plot(xnew,power_smooth)
    plt.show()


    fig.tight_layout()
    # plt.savefig('sum_sizes_vs_fml_len.pdf')
    plt.savefig('success_rates_by_length.pdf')
    # print(df.loc[:,['automaton_sizes','subformulae_sizes']]).head()
    # print(pairs)



def syft_vec_solved_problems(df,L):
    success_vec = [0]*100
    
    length_df = df.loc[df['length'] == "Length %s " % L]
    
    for index, row in length_df.iterrows():
        experiment = row['experiment']
        experiment_number = int(experiment.split(' ')[2].strip().replace(':',''))
        
        if row['status'] == " realizable":
            success_vec[experiment_number] = 1
        elif row['status'] == " unrealizable":
            success_vec[experiment_number] = 1

    return success_vec


def synkit_vec_solved_problems(df):
    success_vec = [0]*100
    
    for index, row in df.iterrows():
        experiment = row['problem']
        # experiment_number = int(experiment.split('_')[1].strip().replace('.sas',''))
        experiment_number = int(experiment.split('_')[1].split('.')[0].replace('.sas',''))
        
        if row['status'] == "R":
            success_vec[experiment_number] = 1
        elif row['status'] == "U":
            success_vec[experiment_number] = 1

    return success_vec


def elementwise_or(v1,v2):
    assert(len(v1) == len(v2))
    result = [0]*len(v1)
    for i in range(len(v1)):
        if v1[i] or v2[i]:
            result[i] = 1
    return result






def plot_one_line(ax,plt,datapoints,l,m,legend):
    plt.scatter(*zip(*datapoints),marker=m)
    ax.set_xlim(1,6)
    ax.set_ylim(0,100)
    # lims = [
    # np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    # np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    # ]
    
    # now plot both limits against eachother
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    from scipy.interpolate import spline
    
    xnew = np.linspace(1,6,300) #300 represents number of points to make between T.min and T.max
    data = zip(*datapoints)
    power_smooth = spline(data[0],data[1],xnew)
    plt.plot(xnew,power_smooth,linestyle=l, label=legend,linewidth=2)
    plt.show()


# synkit_results = []
# for L in range(1,7):
#     synkit_results_L = pd.read_csv('RESULTS/mynd-4GB-30min-L%s-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
#     synkit_results_L.columns = ['domain','problem','runtime','status','errors']
#     # results.index_col = 'config'
#     synkit_results.append(synkit_results_L)
    
# synkit_u_results = []
# for L in range(1,7):
#     synkit_u_results_L = pd.read_csv('RESULTS/mynd_sc-4GB-30min-L%s-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
#     synkit_u_results_L.columns = ['domain','problem','runtime','status','errors']
#     synkit_u_results.append(synkit_u_results_L)

def read_all_results():
    # read data
    all_results = pd.read_csv('L1.stats',  header=None, sep='\t')
    all_results.columns = ['bigprob','L','fmlsize','autsize', 'auttime', 'num_variables','num_actions']
    all_L = [1]*int(len(all_results)/2)

    for L in range(2,7):
        L_results = pd.read_csv('L{}.stats'.format(L),  header=None, sep='\t')
        L_results.columns = ['bigprob','L','fmlsize','autsize','auttime', 'num_variables','num_actions']
                    
        all_results = all_results.append(L_results)
        all_L.extend([L]*int(len(L_results)/2))
    
    all_L = np.array(all_L)
    
    return all_results, all_L

def make_plot_sumautsize_vs_fmlsize():

    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_sumautsize_vs_fmlsize_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    ax.set_xlim(0,700)
    ax.set_ylim(0,240)
    # plt.xlabel('size of the formula')
    # plt.ylabel('sum of automata sizes')
    
    plot_scatter(ax,plt,decomposed_pairs,'-.', 'o', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("sumautsize_vs_fmlsize_decomposed_pairs.pdf")


    # draw plot autsize_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    ax.set_xlim(0,700)
    ax.set_ylim(0,250)
    plt.xlabel('formula size')
    plt.ylabel('automata size')

    plot_scatter(ax,plt,compact_pairs,'-.', 'o', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("sumautsize_vs_fmlsize_compact_pairs.pdf")
    

def make_plot_avgautsize_vs_fmlsize():

    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_avgautsize_vs_fmlsize_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    # ax.set_xlim(1,6)
    # ax.set_ylim(0,100)
    # plt.xlabel('size of the formula')
    # plt.ylabel('sum of automata sizes')
    
    plot_scatter(ax,plt,decomposed_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("avgautsize_vs_fmlsize_decomposed_pairs.pdf")


    # draw plot autsize_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,compact_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("avgautsize_vs_fmlsize_compact_pairs.pdf")
    
    

def make_plot_maxautsize_vs_fmlsize():

    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_maxautsize_vs_fmlsize_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    # ax.set_xlim(1,6)
    # ax.set_ylim(0,100)
    # plt.xlabel('size of the formula')
    # plt.ylabel('sum of automata sizes')
    
    plot_scatter(ax,plt,decomposed_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("maxautsize_vs_fmlsize_decomposed_pairs.pdf")


    # draw plot autsize_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,compact_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("maxautsize_vs_fmlsize_compact_pairs.pdf")
    
def make_plot_auttime_vs_fmlsize():

    # read data
    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_auttime_vs_fmlsize_pairs(all_results)

    # draw plot auttime_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,decomposed_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_decomposed_pairs.pdf")

    # draw plot auttime_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,compact_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_compact_pairs.pdf")
    

def backup_2_make_plot_auttime_vs_fmlsize():

    fig, ax = plt.subplots(figsize=figsize)
    # read data
    for L in range(1,7):
        L_results = pd.read_csv('L{}.stats'.format(L),  header=None, sep='\t')
        L_results.columns = ['bigprob','L','fmlsize','autsize','auttime', 'num_variables','num_actions']
    
        decomposed_pairs,compact_pairs = get_auttime_vs_fmlsize_pairs(L_results)

        # draw plot auttime_vs_fmlsize_decomposed_pairs
        plot_scatter(ax,plt,decomposed_pairs,'-.', markers[L], "Hard decomposition",[L]*len(decomposed_pairs))
        fig.tight_layout()
        # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_decomposed_pairs.pdf")

    # draw plot auttime_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    # read data
    for L in range(1,7):
        L_results = pd.read_csv('L{}.stats'.format(L),  header=None, sep='\t')
        L_results.columns = ['bigprob','L','fmlsize','autsize','auttime', 'num_variables','num_actions']
    
        decomposed_pairs,compact_pairs = get_auttime_vs_fmlsize_pairs(L_results)

        plot_scatter(ax,plt,compact_pairs,'-.', markers[L], "Hard decomposition",[L]*len(decomposed_pairs))
        fig.tight_layout()
        # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_compact_pairs.pdf")
    
def backup_make_plot_auttime_vs_fmlsize():

    # read data
    all_results = pd.read_csv('L1.stats',  header=None, sep='\t')
    all_results.columns = ['bigprob','L','fmlsize','autsize', 'auttime', 'num_variables','num_actions']
    
    for L in range(1,7):
        L_results = pd.read_csv('L{}.stats'.format(L),  header=None, sep='\t')
        L_results.columns = ['bigprob','L','fmlsize','autsize','auttime', 'num_variables','num_actions']
                    
        all_results = all_results.append(L_results)
    
    decomposed_pairs,compact_pairs = get_auttime_vs_fmlsize_pairs(all_results)

    # draw plot auttime_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,decomposed_pairs,'-.', '>', "Hard decomposition")
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_decomposed_pairs.pdf")

    # draw plot auttime_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,compact_pairs,'-.', '>', "Hard decomposition")
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("auttime_vs_fmlsize_compact_pairs.pdf")
    

def plot_scatter(ax,plt,datapoints,l,m,legend,t=False):
    
    plt.scatter(*zip(*datapoints),marker=m)
    plt.show()
    return
    
    plt.scatter(*zip(*datapoints),marker=m,c=t)
    plt.show()
    return

    # else:
    #     plt.scatter(*zip(*datapoints),marker=m)
    
    # # formatting
    # ax.set_xlim(1,6)
    # ax.set_ylim(0,100)
    # lims = [
    # np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    # np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    # ]
    
    # now plot both limits against eachother
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    from scipy.interpolate import spline
    
    xnew = np.linspace(1,6,300) #300 represents number of points to make between T.min and T.max
    data = zip(*datapoints)
    
    power_smooth = spline(data[0],data[1],xnew)
    plt.plot(xnew,power_smooth,linestyle=l, label=legend,linewidth=2)
    plt.show()
    # print(data);exit()
    
def get_sumautsize_vs_fmlsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        sumautsize = sum(int(s) for s in row['autsize'].split(','))
        pair = (row['fmlsize'],sumautsize)
        # print(pair)
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs

def get_avgautsize_vs_fmlsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        autsizes = [int(s) for s in row['autsize'].split(',')]
        avgautsize = float(sum(autsizes))/len(autsizes)
        pair = (row['fmlsize'],avgautsize)
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs
    
def get_maxautsize_vs_fmlsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        autsizes = [int(s) for s in row['autsize'].split(',')]
        maxautsize = max(autsizes)
        pair = (row['fmlsize'],maxautsize)
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs
    
def get_auttime_vs_fmlsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        pair = (row['fmlsize'],row['auttime'])
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs
    

def get_saspsize_vs_autsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        num_variables = row['num_variables']
        num_actions = row['num_actions']
        autsizes = [int(s) for s in row['autsize'].split(',')]
        # avgautsize = float(sum(autsizes))/len(autsizes)
        sumautsize = sum(autsizes)
        saspsize = num_variables + num_actions
        pair = (sumautsize, saspsize)
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs
    

def get_saspsize_vs_fmlsize_pairs(df):
    compact_pairs = []
    decomposed_pairs = []
    for index, row in df.iterrows():
        bigprob = row['bigprob']
        num_variables = row['num_variables']
        num_actions = row['num_actions']
        pair = (row['fmlsize'],num_variables+num_actions)
        if "compact" in bigprob:
            compact_pairs.append(pair)
        else:
            decomposed_pairs.append(pair)
    return decomposed_pairs,compact_pairs
    
    
def make_plot_saspsize_vs_autsize():

    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_saspsize_vs_autsize_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    ax.set_xlim(1,250)
    ax.set_ylim(1,15000)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.xlabel('automata size')
    plt.ylabel('FOND size')

    plot_scatter(ax,plt,decomposed_pairs,'-.', 'o', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("saspsize_vs_autsize_decomposed_pairs.pdf")


    # draw plot autsize_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    ax.set_xlim(1,250)
    ax.set_ylim(1,15000)
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.xlabel('automata size')
    plt.ylabel('FOND problem size')
    
    lims = [
    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    ]
    
    # now plot both limits against eachother
    ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    plot_scatter(ax,plt,compact_pairs,'-.', 'o', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("saspsize_vs_autsize_compact_pairs.pdf")
    
def make_plot_saspsize_vs_fmlsize():

    all_results, all_L = read_all_results()
    decomposed_pairs,compact_pairs = get_saspsize_vs_fmlsize_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    # ax.set_xlim(1,6)
    # ax.set_ylim(0,100)
    # plt.xlabel('size of the formula')
    # plt.ylabel('sum of automata sizes')
    
    plot_scatter(ax,plt,decomposed_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("saspsize_vs_fmlsize_decomposed_pairs.pdf")


    # draw plot autsize_vs_fmlsize_compact_pairs
    fig, ax = plt.subplots(figsize=figsize)
    plot_scatter(ax,plt,compact_pairs,'-.', '>', "Hard decomposition",all_L)
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("saspsize_vs_fmlsize_compact_pairs.pdf")
        

def get_saspsize_comparison_pairs(df):
    pairs = []
    
    for index, row in df.iterrows():
        bigprob = row['bigprob']

        if "compact" in bigprob:
            bigprob = row['bigprob']
            num_variables = row['num_variables']
            num_actions = row['num_actions']
            saspsize_compact = num_variables + num_actions
            
            # print(bigprob)
            
            for i, r in df.iterrows():
                if r['bigprob'] == bigprob.replace(".compact",'') and r['L'] == row['L']:
                    num_variables = r['num_variables']
                    num_actions = r['num_actions']
                    saspsize_decomposed = num_variables + num_actions
                    break
            
            pair = (saspsize_decomposed,saspsize_compact)
            pairs.append(pair)
        else:
            continue

    return pairs

def make_plot_saspsize_comparison():

    all_results, all_L = read_all_results()
    pairs = get_saspsize_comparison_pairs(all_results)

    # draw plot autsize_vs_fmlsize_decomposed_pairs
    fig, ax = plt.subplots(figsize=figsize)
    # ax.set_xlim(1,6)
    # ax.set_ylim(0,100)
    # plt.xlabel('size of the formula')
    # plt.ylabel('sum of automata sizes')

    
    fig, ax = plt.subplots(figsize=figsize)
    # plt.plot(success_vec)
    plt.scatter(*zip(*pairs),marker='o')
    ax.set_xlim(10,15000)
    ax.set_ylim(10,15000)
    ax.set_xscale('log')
    ax.set_yscale('log')
    # plt.set_aspect('equal')
    # plt.plot([0, 1], [0, 1], transform=plt.transAxes)
    plt.xlabel('hard decomposition')
    plt.ylabel('soft decomposition')
    
    lims = [
    np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    ]
    
    # now plot both limits against eachother
    ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    
    
    fig.tight_layout()
    # save the two figures to files
    fig.savefig("saspsize_comparison.pdf")

    
def make_all_plots():
    syft_datapoints = []
    synkit_datapoints = []
    synkit_u_datapoints = []
    cumulated_synkit_datapoints = []
    cumulated_syft_synkit_datapoints = []
    
    
    syft_results = pd.read_csv('syft_results.txt',  header=None, sep='\t')
    syft_results.columns = ['length','experiment','ltlf2fol','mona','syft','status']
    # results.index_col = 'config'

    
    for L in range(1,7):
        synkit_results_L = synkit_results[L-1]
        synkit_u_results_L = synkit_u_results[L-1]
        # synkit_results = pd.read_csv('RESULTS/mynd-L%s_realizability-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
        # synkit_results.columns = ['domain','problem','runtime','status','errors']
        # # results.index_col = 'config'

        # synkit_u_results = pd.read_csv('RESULTS/mynd-L%s_unrealizability-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
        # synkit_u_results.columns = ['domain','problem','runtime','status','errors']

        
        syft_vec_L = syft_vec_solved_problems(syft_results,L)
        synkit_vec_L = synkit_vec_solved_problems(synkit_results_L)
        synkit_u_vec_L = synkit_vec_solved_problems(synkit_u_results_L)
        cumulated_synkit_vec_L = elementwise_or(synkit_vec_L,synkit_u_vec_L)
        cumulated_syft_synkit_vec_L = elementwise_or(elementwise_or(syft_vec_L, synkit_vec_L),synkit_u_vec_L)
        
        syft_datapoint = (L,sum(syft_vec_L))
        synkit_datapoint = (L,sum(synkit_vec_L))
        synkit_u_datapoint = (L,sum(synkit_u_vec_L))
        cumulated_synkit_datapoint = (L,sum(cumulated_synkit_vec_L))
        cumulated_syft_synkit_datapoint = (L,sum(cumulated_syft_synkit_vec_L))

        syft_datapoints.append(syft_datapoint)
        synkit_datapoints.append(synkit_datapoint)
        synkit_u_datapoints.append(synkit_u_datapoint)
        cumulated_synkit_datapoints.append(cumulated_synkit_datapoint)
        cumulated_syft_synkit_datapoints.append(cumulated_syft_synkit_datapoint)

    
    
    fig, ax = plt.subplots(figsize=figsize)
    
    plot_one_line(ax,plt,cumulated_syft_synkit_datapoints,'-.', '>', "Syft+SynKit")
    plot_one_line(ax,plt,syft_datapoints,'--', 'o', "Syft")
    # plot_one_line(ax,plt,cumulated_synkit_datapoints,'-.', '^', "SynKit(real) + SynKit(unreal)")
    plot_one_line(ax,plt,cumulated_synkit_datapoints,':', '^', "SynKit")
    # plot_one_line(ax,plt,synkit_u_datapoints,':', '*', "SynKit(unreal)")
    # plot_one_line(ax,plt,synkit_datapoints,':', 'v', "SynKit(real)")
    
    # plt.set_aspect('equal')
    # plt.plot([0, 1], [0, 1], transform=plt.transAxes)
    plt.xlabel('problem size')
    plt.ylabel('\#problems')
    

    legend = ax.legend(loc='lower left', shadow=False)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        # label.set_fontsize('small')
        label.set_fontsize(16)
    
    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width
        
    fig.tight_layout()
    # plt.savefig('sum_sizes_vs_fml_len.pdf')
    plt.savefig('all_plots_realizability.pdf')
    # print(df.loc[:,['automaton_sizes','subformulae_sizes']]).head()
    # print(pairs)


    import pylab
    # create a second figure for the legend
    figLegend = pylab.figure(figsize = (4,3))
    figLegend.suptitle('Legend (ordered by performance)', fontsize=20)
    # produce a legend for the objects in the other figure
    pylab.figlegend(*ax.get_legend_handles_labels(), loc = 'center', fontsize = 16)
    
    fig.tight_layout()
    # save the two figures to files
    figLegend.savefig("legend_realizability.pdf")


# make_all_plots()

# make_plot_sumautsize_vs_fmlsize()
# make_plot_avgautsize_vs_fmlsize()
# make_plot_maxautsize_vs_fmlsize()
# make_plot_auttime_vs_fmlsize()


# make_plot_saspsize_vs_autsize()
# make_plot_saspsize_vs_fmlsize()


# used in the camera-ready
# make_plot_saspsize_comparison()
make_plot_sumautsize_vs_fmlsize()
# make_plot_saspsize_vs_autsize()