
import os,subprocess

SYFCO = "./src/parser/syfco/syfco "
benchmark_probs_dir = "src/parser/syfco/examples/lily/"
# benchmark_probs_dir = "expfiles_icaps18/benchmarks/"

def acacia2ltlxba(spec):
    assumptions = []
    guarantees = []
    for constraint in spec.split(';'):
        if "assume" in constraint:
            assumptions.append( constraint.replace("assume ","").strip() )
        elif len(constraint.strip() ) > 0:
            guarantees.append( constraint.strip() )
    if len(assumptions) > 0:
        ltl_spec = "( %s ) -> ( %s ) " % ( " && ".join('(%s)' % a for a in assumptions),  " && ".join('(%s)' % g for g in guarantees)  )
        # ltl_spec = "(!( %s )) || ( %s ) " % ( " && ".join(assumptions),  " && ".join(guarantees)  )
    else:
        ltl_spec = " && ".join('(%s)' % g for g in guarantees) 
    # print(ltl_spec)
    ltl_spec = ltl_spec.replace('*','&&').replace('+','||')
    # return ltl_spec.replace("\n","")
    return ltl_spec.strip()

def create_base_files():
    # creates .part, .ltl, and .ltl_part files from Acacia benchmark problems,
    # these will be used as a base to create larger problems.
    if not exists("expfiles_icaps18/base/"):
        makedirs("expfiles_icaps18/base/")
    
    for filename in os.listdir(benchmark_probs_dir):
        print(filename)
        cmd = "{} -f acacia-specs {} -m fully -pf expfiles_icaps18/base/{}.part".format(SYFCO, os.path.join(benchmark_probs_dir,filename),filename)
        
        p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        spec = str(out, 'utf-8').strip()
        # print(spec)
        
        # output .part and .ltl_specs files with one conjunct per line
        
        specs = []
        spec_unit_idx = 0
        init_idx = spec.find( "[spec_unit %s]" % spec_unit_idx ) + len("[spec_unit %s]" % spec_unit_idx)
        spec_unit_idx += 1
        end_idx = spec.find( "[spec_unit %s]" % spec_unit_idx )
        while( end_idx != -1):
            spec_unit = acacia2ltlxba(spec[init_idx:end_idx])
            specs.append(spec_unit)
            init_idx = spec.find( "[spec_unit %s]" % spec_unit_idx ) + len("[spec_unit %s]" % spec_unit_idx)
            spec_unit_idx += 1
            end_idx = spec.find( "[spec_unit %s]" % spec_unit_idx )
        spec_unit = acacia2ltlxba(spec[init_idx:])
        specs.append(spec_unit)
        
        # print(specs)
        # exit()
        
        ltl_specs_filename = "expfiles_icaps18/base/{}.ltl_specs".format(filename)
        f = open(ltl_specs_filename, 'w')
        f.write('\n'.join(specs))
        f.close()
    
    
        # output ltl file with one line containing all conjuncts
        
        cmd = "{} -f ltlxba {} -m fully -o expfiles_icaps18/base/{}.ltl".format(SYFCO, os.path.join(benchmark_probs_dir,filename),filename)
    
        p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        spec = str(out, 'utf-8').strip()
        
        
        
from os import listdir, makedirs
from os.path import isfile, join, exists
import random
from random import randint
random.seed(387)

def get_fml_size(ltl_file):
    fml_size = 0
    keywords = ['&&','||','G','F','R','W','X','U',' ->','<->']
    f = open(ltl_file,'r')
    for line in f.readlines():
        for keyword in keywords:
            fml_size += line.count(keyword)
    f.close()
    return fml_size

# modified from krrt.utils
# bitbucket.org/haz/krtoolkit
def get_value(text, regex, value_type = float):
    """ Lifts a value from a file based on a regex passed in. """
    import re
    # Get the text of the time
    pattern = re.compile(regex, re.MULTILINE)
    results = pattern.search(text)
    if results:
        return value_type(results.group(1))
    else:
        print("Could not find the value, %s, in the text provided" % (regex))
        return(value_type(0.0))
        
        
def get_automata_stats(out):
    # n_automata = out.count("Generated nfw automaton")
    sum_aut_sizes = 0
    aut_sizes = []
    lines = out.split('\n')
    for line in lines:
        if "Generated nfw automaton" in line:
            aut_size = int(line.strip().split()[-1])
            sum_aut_sizes += aut_size
            aut_sizes.append(aut_size)
        
    sum_automata_time = get_value(out, '.*Automata generation time:[\s]*([0-9]+\.?[0-9]*).\n.*', float)
    # print(sum_aut_sizes)
    # print(sum_automata_time)
    # print(out)
    # return (sum_aut_sizes,sum_automata_time)
    return (aut_sizes,sum_automata_time)
    
def get_sasp_stats(sasp_file):
    f = open(sasp_file,'r')
    sasp_problem = f.read()
    
    num_actions = sasp_problem.count("begin_operator")
    num_variables = sasp_problem.count("begin_variable")
    f.close()
    return (num_actions,num_variables)

def make_tlsf(inputs,outputs,subformulas,experiment_files):
    
    header_block = """INFO {
  TITLE:       "%s"
  DESCRIPTION: "Big problem obtained from logical composition of smaller subproblems"
  SEMANTICS:   Mealy
  TARGET:      Mealy
}""" % ' '.join(experiment_files)

    inputs_subblock = "INPUTS {\n\t%s;\n}" % ';\n\t'.join(var.lower() for var in inputs)
    outputs_subblock = "OUTPUTS {\n\t%s;\n}" % ';\n\t'.join(var.lower() for var in outputs)
    for var in inputs+outputs+["TRUE", "FALSE"]:
        for i in range(len(subformulas)):
            subformulas[i] = subformulas[i].replace(var, var.lower())
    guarantees_subblock = "GUARANTEES {\n\t%s;\n}" % ';\n\t'.join(subformulas)

    
    main_block = "MAIN {\n%s\n%s\n%s\n}" % (inputs_subblock, outputs_subblock, guarantees_subblock)
  
    tlsf = "%s\n%s" % (header_block, main_block)
    return tlsf



def read_inputs(part_file):
    f = open(part_file,"r")
    for line in f.readlines():
        if line.startswith(".inputs"):
            inputs = line.strip().split()
            # inputs = [var.upper().strip() for var in inputs[1:]]
            inputs = [var.strip() for var in inputs[1:]]
            f.close()
            return inputs

def read_outputs(part_file):
    f = open(part_file,"r")
    for line in f.readlines():
        if line.startswith(".outputs"):
            inputs = line.strip().split()
            # inputs = [var.upper().strip() for var in inputs[1:]]
            inputs = [var.strip() for var in inputs[1:]]
            f.close()
            return inputs
        
def read_formula(ltl_file):
    f = open(ltl_file,"r")
    text = f.read()
    fml = text.strip().replace(';','')#.upper()
    f.close()
    return fml

def normalize_formula(fml,inputs,outputs):
    for var in inputs+outputs:
        fml = fml.replace("%s=1" % var,"%s" % var)
        fml = fml.replace("%s = 1" % var,"%s" % var)
        fml = fml.replace("%s =1" % var,"%s" % var)
        fml = fml.replace("%s= 1" % var,"%s" % var)
        fml = fml.replace("%s=0" % var,"(!%s)" % var)
        fml = fml.replace("%s = 0" % var,"(!%s)" % var)
        fml = fml.replace("%s= 0" % var,"(!%s)" % var)
        fml = fml.replace("%s =0" % var,"(!%s)" % var)

        # fml = fml.replace("->"," -> ")
        fml = "(%s)" % fml
    return fml
        

def reduce_input_vars(cur_n_input_vars,final_n_input_vars,subformulas):
    new_subformulas = list(subformulas)
    for n in range(cur_n_input_vars):
        idx = randint(0,final_n_input_vars-1)
        for i in range(len(new_subformulas)): 
            new_subformulas[i] = new_subformulas[i].replace('INPUT_%s_VAR' % n, 'TEMPKW_%s_VAR' % idx)
            
    for i in range(len(new_subformulas)): 
        new_subformulas[i] = new_subformulas[i].replace('TEMPKW_', 'INPUT_')

    return new_subformulas

def reduce_output_vars(cur_n_input_vars,final_n_input_vars,subformulas):
    new_subformulas = list(subformulas)
    for n in range(cur_n_input_vars):
        idx = randint(0,final_n_input_vars-1)
        for i in range(len(new_subformulas)): 
            new_subformulas[i] = new_subformulas[i].replace('OUTPUT_%s_VAR' % n, 'TEMPKW_%s_VAR' % idx)
            
    for i in range(len(new_subformulas)): 
        new_subformulas[i] = new_subformulas[i].replace('TEMPKW_', 'OUTPUT_')

    return new_subformulas

    
# def reduce_input_vars(cur_n_input_vars,final_n_input_vars,final_formula):
#     for n in range(cur_n_input_vars):
#         idx = randint(0,final_n_input_vars-1)
#         final_formula = final_formula.replace('INPUT_%s_VAR' % n, 'TEMPKW_%s_VAR' % idx)
#     final_formula = final_formula.replace('TEMPKW_', 'INPUT_')
#     return final_formula

# def reduce_output_vars(cur_n_output_vars,final_n_output_vars,final_formula):
#     for n in range(cur_n_output_vars):
#         idx = randint(0,final_n_output_vars-1)
#         final_formula = final_formula.replace('OUTPUT_%s_VAR' % n, 'TEMPKW_%s_VAR' % idx)
#     final_formula = final_formula.replace('TEMPKW_', 'OUTPUT_')
#     return final_formula


def generate_conjunction_TLSF_PART_LTL_files(L):
    # generates .ltl and .part files (for Syft)
    # generates .TLSF files (for SynKit)
    import os
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(cur_dir, "expfiles_icaps18/base")
    formula_length = L
    num_experiments = 100
    vars_reduction_factor = 1
    total_final_n_vars = None
    output_path = os.path.join(os.path.join(cur_dir, "expfiles_icaps18"), "L%s" % L)
    
    
    if not exists(output_path):
        makedirs(output_path)
    
    
    ltl_files = sorted([f for f in listdir(input_path) if isfile(join(input_path, f)) and not f.startswith('._') and f.endswith('.ltl')])
    # part_files = [f for f in listdir(input_path) if isfile(join(input_path, f)) and not f.startswith('._') and f.endswith('.part')]
    files_noextension = [f.replace('.ltl','') for f in ltl_files]
    
    
    
    for experiment_number in range(num_experiments):
    
        experiment_name = "bigprob_%s" % experiment_number
        input_var_idx = 0
        output_var_idx = 0
        experiment_subformulas = []
        experiment_files = []
        # experiment_inputs = []
        # experiment_outputs = []
        experiment_subformulas_ltl_specs = []
        for k in range(formula_length):
            idx = randint(0,len(files_noextension)-1)
            f = files_noextension[idx]
            experiment_files.append(f)
            
            f_inputs = read_inputs(join(input_path, f+'.part'))
            f_outputs = read_outputs(join(input_path, f+'.part'))
            f_formula = read_formula(join(input_path, f+'.ltl'))

            f_formula = normalize_formula(f_formula,f_inputs,f_outputs)
            
            f_formula_specs = read_formula(join(input_path, f+'.ltl_specs')).split('\n')
            f_formula_specs = [normalize_formula(fml,f_inputs,f_outputs) for fml in f_formula_specs]
            
            for f_var in f_inputs:
                # experiment_var = 'VAR_%s' % input_var_idx
                # f_formula = f_formula.replace(f_var, 'INPUT_%s_VAR' % input_var_idx)
                f_formula = f_formula.replace(f_var, 'INPUT_%s_VAR' % input_var_idx)
                # experiment_inputs.append(experiment_var)
                f_formula_specs = [fml.replace(f_var, 'INPUT_%s_VAR' % input_var_idx) for fml in f_formula_specs]
                input_var_idx += 1
            
            for f_var in f_outputs:
                # experiment_var = 'VAR_%s' % output_var_idx
                f_formula = f_formula.replace(f_var, 'OUTPUT_%s_VAR' % output_var_idx)
                # experiment_outputs.append(experiment_var)
                f_formula_specs = [fml.replace(f_var, 'OUTPUT_%s_VAR' % output_var_idx) for fml in f_formula_specs]
                output_var_idx += 1
            
            experiment_subformulas.append(f_formula)
            # experiment_subformulas_ltl_specs.append(f_formula_specs)
            experiment_subformulas_ltl_specs.extend(f_formula_specs)
        
        # final_formula = ' && '.join( "(%s)" % fml for fml in experiment_subformulas)
        
        
        # optional: collapse varibles
        if False:
            cur_n_input_vars = input_var_idx
            cur_n_output_vars = output_var_idx
            if vars_reduction_factor != None:
                final_n_input_vars = input_var_idx/vars_reduction_factor
                final_n_output_vars = output_var_idx/vars_reduction_factor
            else:
                assert(total_final_n_vars != None)
                final_n_input_vars = total_final_n_vars * input_var_idx/(input_var_idx+output_var_idx)
                final_n_output_vars = total_final_n_vars * output_var_idx/(input_var_idx+output_var_idx)
            # print(final_n_input_vars)
            # print(final_n_output_vars)
            experiment_subformulas = reduce_input_vars(cur_n_input_vars,final_n_input_vars,experiment_subformulas)
            experiment_subformulas = reduce_output_vars(cur_n_output_vars,final_n_output_vars,experiment_subformulas)
            # final_formula = reduce_input_vars(cur_n_input_vars,final_n_input_vars,final_formula)
            # final_formula = reduce_output_vars(cur_n_output_vars,final_n_output_vars,final_formula)
        else:
            cur_n_input_vars = input_var_idx
            cur_n_output_vars = output_var_idx
            final_n_input_vars = input_var_idx
            final_n_output_vars = output_var_idx
    
        
        experiment_ltl_file = join(output_path, experiment_name +'.ltl')
        f = open(experiment_ltl_file,"w")
        
        # the final formula is random conjunction of disjunction of subformulas
        final_formula = experiment_subformulas[0]
        for k in range(1,len(experiment_subformulas)):
            subformula = experiment_subformulas[k]
            # final_formula = "(%s%s%s)" %(final_formula,random.choice([" && ", " || "]), subformula)
            final_formula = "(%s%s%s)" %(final_formula, " && ", subformula)

        # final_formula = random.choice(["&&", "||"]).join( "(%s)" % fml for fml in experiment_subformulas)
        # final_formula = ' || '.join( "(%s)" % fml for fml in experiment_subformulas)
        
        # f.write(final_formula.upper())
        f.write(final_formula)
        f.close()
        # print(experiment_subformulas)
        experiment_part_file = join(output_path, experiment_name +'.part')
        f = open(experiment_part_file,"w")
        
        f.write(".inputs %s\n" % ' '.join("INPUT_%s_VAR" % n for n in range(final_n_input_vars)))
        f.write(".outputs %s\n" % ' '.join("OUTPUT_%s_VAR" % n for n in range(final_n_output_vars)))
        
        f.close()
        
        # write TLSF
        experiment_tlsf_file = join(output_path, experiment_name +'.tlsf')
        f = open(experiment_tlsf_file,"w")
        inputs = ["INPUT_%s_VAR" % n for n in range(final_n_input_vars)]
        outputs = ["OUTPUT_%s_VAR" % n for n in range(final_n_output_vars)]
        # tlsf = make_tlsf(inputs,outputs,experiment_subformulas,experiment_files)
        # tlsf = make_tlsf(inputs,outputs,[final_formula],experiment_files)
        tlsf = make_tlsf(inputs,outputs,experiment_subformulas_ltl_specs,experiment_files)
        f.write(tlsf)
        f.close()
        # print(tlsf)
        
        if True: #these yield bigger SASp files
            experiment_compact_tlsf_file = join(output_path, experiment_name +'.compact.tlsf')
            f = open(experiment_compact_tlsf_file,"w")
            tlsf = make_tlsf(inputs,outputs,experiment_subformulas,experiment_files)
            f.write(tlsf)
            f.close()
            # print(tlsf)
            # exit()
            
            # experiment_single_tlsf_file = join(output_path, experiment_name +'.single.tlsf')
            # f = open(experiment_single_tlsf_file,"w")
            # tlsf = make_tlsf(inputs,outputs,[final_formula],experiment_files)
            # f.write(tlsf)
            # f.close()


def generate_SASp_files(L,generate_stats_files=True):
    import os
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(cur_dir, "expfiles_icaps18/base")
    formula_length = L
    input_path = os.path.join(os.path.join(cur_dir, "expfiles_icaps18"), "L%s" % L)
    output_path = input_path
    tlsf_files = sorted([f for f in listdir(input_path) if isfile(join(input_path, f)) and not f.startswith('._') and f.endswith('.tlsf')])
    
    
    for tlsf_file in tlsf_files:
        # generate SASp file for realizablity
        in_file = os.path.join(input_path,tlsf_file)
        out_file_prefix = os.path.join(output_path, tlsf_file+'.realizability')
        
        cmd = "python3 ../workspace/src/synkit-fin.py --config NFWAutomaton --tlsf {} --from-ltlf Yes --split-spec icaps18tests_components --aut-type nfw --fixed-var-ordering True --output-prefix {} --planner mynd --planner-path ../workspace/src/planner/mynd/src --semantics mealy --game-semantics mealy --game-type nfw --check-unrealizability False".format(in_file, out_file_prefix)
        print(cmd)
        p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()

        sas_in_file = os.path.join(output_path, tlsf_file+'.realizability_domain.pddl')
        sas_out_file = os.path.join(output_path, tlsf_file+'.realizability.sas')
        # print(sas_in_file)
        os.rename(sas_in_file,sas_out_file)
        
        try:
            # generate SASp file for unrealizablity
            in_file = os.path.join(input_path,tlsf_file)
            out_file_prefix = os.path.join(output_path, tlsf_file+'.unrealizability')
    
            cmd = "python3 ../workspace/src/synkit-fin.py --config NFWAutomaton --tlsf {} --from-ltlf Yes --split-spec icaps18tests_components --aut-type nfw --fixed-var-ordering True --output-prefix {} --planner mynd_sc --planner-path ../workspace/src/planner/mynd/src --semantics mealy --game-semantics moore --game-type u0cw --check-unrealizability True".format(in_file, out_file_prefix)
            # print(cmd)
            # exit()
            p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            out, err = p.communicate()
    
            sas_in_file = os.path.join(output_path, tlsf_file+'.unrealizability_domain.pddl')
            sas_out_file = os.path.join(output_path, tlsf_file+'.unrealizability.sas')
            print(sas_in_file)
            os.rename(sas_in_file,sas_out_file)
        except:
            print("No SAS file generated")
            # prefix = ltl_file.replace('.ltl','')
            prefix = tlsf_file.replace('.tlsf','')
            path_prefix = os.path.join('expfiles_icaps18/' + os.path.basename(output_path), prefix)
            # print(os.path.basename(output_path));exit()
            cmd = "rm {}.tlsf.unrealizability.sas;".format(path_prefix)
            # print(cmd);exit()
            p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            out, err = p.communicate()
            continue

        if generate_stats_files:
            try:
                if tlsf_file.endswith('.compact.tlsf'):
                    ltl_file = os.path.join(output_path, tlsf_file.replace('.compact.tlsf', '.ltl'))
                elif tlsf_file.endswith('.single.tlsf'):
                    ltl_file = os.path.join(output_path, tlsf_file.replace('.single.tlsf', '.ltl'))
                else:
                    assert(tlsf_file.endswith('.tlsf'))
                    ltl_file = os.path.join(output_path, tlsf_file.replace('.tlsf', '.ltl'))
                    
                fml_size = get_fml_size(ltl_file)
                # sum_automata_sizes, sum_aut_time = get_automata_stats(out)
                automata_sizes, sum_aut_time = get_automata_stats(out)
                # print(automata_sizes)
                str_automata_sizes = ','.join(str(s) for s in automata_sizes)
                # print(str_automata_sizes);exit()
                sasp_file = sas_out_file
                num_actions,num_variables = get_sasp_stats(sasp_file)
    
                stats_file = os.path.join(output_path, tlsf_file.replace('.tlsf', '.stats'))
                

                print("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(tlsf_file,L,fml_size,str_automata_sizes,sum_aut_time,num_actions,num_variables))
                
                f = open(stats_file,'w')
                f.write("tlsf_file\tL\tfml_size\tautomata_sizes\tsum_aut_time\tnum_actions\tnum_variables\n")
                f.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(tlsf_file,L,fml_size,str_automata_sizes,sum_aut_time,num_actions,num_variables))
                f.close()
            except:
                print("Error in creating stats file")
            
            # prefix = ltl_file.replace('.ltl','')
            prefix = tlsf_file.replace('.tlsf','')
            path_prefix = os.path.join('expfiles_icaps18/' + os.path.basename(output_path), prefix)
            # print(os.path.basename(output_path));exit()
            cmd = "tar  -cf {}.tar {}.*; gzip -9 {}.tar; rm {}.tlsf.unrealizability.sas; rm {}.tlsf.realizability.sas; rm {}.tlsf".format(path_prefix,path_prefix,path_prefix,path_prefix,path_prefix,path_prefix)
            # print(cmd);exit()
            p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
            out, err = p.communicate()
            
                
    cmd = "more {}/bigprob_*.stats | grep .tlsf > {}.stats".format(output_path,output_path)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    out, err = p.communicate()

    # cmd = "tar  -cf {}.tar {}; gzip -9 {}.tar; rm -r {}".format(output_path, output_path,output_path,output_path)
    # cmd = "zip -r {}.zip {}".format(output_path,output_path)
    
    # p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    # out, err = p.communicate()
    import shutil
    shutil.make_archive(output_path, 'zip', output_path)
    # os.rmdir(output_path)
    cmd = "rm -r {}".format(output_path)
    p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    
def generate_PDDL_files(L):
    
    import os
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    input_path = os.path.join(cur_dir, "expfiles_icaps18/base")
    formula_length = L
    input_path = os.path.join(os.path.join(cur_dir, "expfiles_icaps18"), "L%s" % L)
    output_path = input_path
    tlsf_files = sorted([f for f in listdir(input_path) if isfile(join(input_path, f)) and not f.startswith('._') and f.endswith('.tlsf')])
    
    
    for tlsf_file in tlsf_files:
        # generate SASp file for unrealizaiblity
        in_file = os.path.join(input_path,tlsf_file)
        out_file_prefix = os.path.join(output_path, tlsf_file+'.unrealizability')

        cmd = "python3 ../workspace/src/synkit-fin.py --config NFWAutomaton --tlsf {} --from-ltlf Yes --split-spec icaps18tests_components --aut-type nfw --fixed-var-ordering True --output-prefix {} --planner prp --planner-path ../workspace/src/planner/prp-ds/src/prp --semantics mealy --game-semantics moore --game-type u0cw --check-unrealizability True".format(in_file, out_file_prefix)
        # print(cmd)
        p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()

        sas_in_file = os.path.join(output_path, tlsf_file+'.unrealizability_domain.pddl')
        sas_out_file = os.path.join(output_path, tlsf_file+'.unrealizability.domain.pddl')
        os.rename(sas_in_file,sas_out_file)
        
        sas_in_file = os.path.join(output_path, tlsf_file+'.unrealizability_instance.pddl')
        sas_out_file = os.path.join(output_path, tlsf_file+'.unrealizability.instance.pddl')
        os.rename(sas_in_file,sas_out_file)

        # exit()
    # cmd = "tar  -cf {}.tar {}; gzip -9 {}.tar; rm -r {}".format(output_path, output_path,output_path,output_path)
    
    # p = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    # out, err = p.communicate()
    
if __name__ == "__main__":
    import sys
    # create_base_files()
    for L in range(1,int(sys.argv[1])+1):
        generate_conjunction_TLSF_PART_LTL_files(L)
        generate_SASp_files(L)
        
    # for L in range(1,int(sys.argv[1])+1):
    #     generate_SASp_files(L)
    #     generate_PDDL_files(L)
    
    # L = int(sys.argv[1])
    # generate_conjunction_TLSF_PART_LTL_files(L)
    # generate_SASp_files(L)
    # generate_PDDL_files(L)
        



