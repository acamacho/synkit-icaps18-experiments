import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import gridspec

#sns.set_palette("Set1", desat=.6)
sns.set_palette('muted')
# sns.set_style("white", {'grid.color': '.95'})
sns.set_style("ticks")
# style : dict, None, or one of {darkgrid, whitegrid, dark, white, ticks}


# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
pd.set_option('display.mpl_style', 'default') # Make the graphs a bit prettier
# figsize(15, 5)
figsize = (4,3)

plt.rc('font', family='serif', serif='Times')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=16)
plt.rc('ytick', labelsize=16)
plt.rc('axes', labelsize=18)

import ast




def success_rates_by_length(df):
    import ast
    success_vec = []
    for length in range(1,7):
        length_df = df.loc[df['length'] == "Length %s " % length]
        realizable_df = length_df.loc[length_df['status'] == " realizable"]
        unrealizable_df = length_df.loc[length_df['status'] == " unrealizable"]
        # print(unrealizable_df.to_string())
        # exit()
        n_success = len( realizable_df ) + len( unrealizable_df)
        success_vec.append((length,n_success))
    
    
    fig, ax = plt.subplots(figsize=figsize)
    # plt.plot(success_vec)
    plt.scatter(*zip(*success_vec))
    ax.set_xlim(0,6)
    ax.set_ylim(0,100)
    # plt.set_aspect('equal')
    # plt.plot([0, 1], [0, 1], transform=plt.transAxes)
    plt.xlabel('number of subproblems')
    plt.ylabel('solved problems')
    
    # lims = [
    # np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    # np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    # ]
    
    # now plot both limits against eachother
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    from scipy.interpolate import spline
    
    xnew = np.linspace(1,6,300) #300 represents number of points to make between T.min and T.max
    data = zip(*success_vec)
    power_smooth = spline(data[0],data[1],xnew)
    plt.plot(xnew,power_smooth)
    plt.show()


    fig.tight_layout()
    # plt.savefig('sum_sizes_vs_fml_len.pdf')
    plt.savefig('success_rates_by_length.pdf')
    # print(df.loc[:,['automaton_sizes','subformulae_sizes']]).head()
    # print(pairs)



def syft_vec_solved_problems(df,L):
    success_vec = [0]*100
    
    length_df = df.loc[df['length'] == "Length %s " % L]
    
    for index, row in length_df.iterrows():
        experiment = row['experiment']
        experiment_number = int(experiment.split(' ')[2].strip().replace(':',''))
        
        if row['status'] == " realizable":
            success_vec[experiment_number] = 1
        elif row['status'] == " unrealizable":
            success_vec[experiment_number] = 1

    return success_vec


def synkit_vec_solved_problems(df):
    success_vec = [0]*100
    
    for index, row in df.iterrows():
        experiment = row['problem']
        # experiment_number = int(experiment.split('_')[1].strip().replace('.sas',''))
        experiment_number = int(experiment.split('_')[1].split('.')[0].replace('.sas',''))
        
        if row['status'] == "R":
            success_vec[experiment_number] = 1
        elif row['status'] == "U":
            success_vec[experiment_number] = 1

    return success_vec


def synkit_compact_vec_solved_problems(df):
    success_vec = [0]*100
    
    for index, row in df.iterrows():
        experiment = row['problem']
        if ".compact." not in experiment:
            continue
        # experiment_number = int(experiment.split('_')[1].strip().replace('.sas',''))
        experiment_number = int(experiment.split('_')[1].split('.')[0].replace('.sas',''))
        
        if row['status'] == "R":
            success_vec[experiment_number] = 1
        elif row['status'] == "U":
            success_vec[experiment_number] = 1

    return success_vec


def synkit_decomposed_vec_solved_problems(df):
    success_vec = [0]*100
    
    for index, row in df.iterrows():
        experiment = row['problem']
        if ".compact." in experiment:
            continue
        # experiment_number = int(experiment.split('_')[1].strip().replace('.sas',''))
        experiment_number = int(experiment.split('_')[1].split('.')[0].replace('.sas',''))
        
        if row['status'] == "R":
            success_vec[experiment_number] = 1
        elif row['status'] == "U":
            success_vec[experiment_number] = 1

    return success_vec

def elementwise_or(v1,v2):
    assert(len(v1) == len(v2))
    result = [0]*len(v1)
    for i in range(len(v1)):
        if v1[i] or v2[i]:
            result[i] = 1
    return result






def plot_one_line(ax,plt,datapoints,l,m,legend,clr=None):
    # plt.scatter(*zip(*datapoints),marker=m)
    if clr != None:
        plt.plot(*zip(*datapoints),marker=m,linestyle=l, label=legend,linewidth=2, c=clr)
    else:
        plt.plot(*zip(*datapoints),marker=m,linestyle=l, label=legend,linewidth=2)
    ax.set_xlim(1,6)
    ax.set_ylim(0,110)
    # lims = [
    # np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
    # np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
    # ]
    
    # now plot both limits against eachother
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    # from scipy.interpolate import spline
    
    # # xnew = np.linspace(1,6,300) #300 represents number of points to make between T.min and T.max
    # xnew = np.linspace(1,6,6) #300 represents number of points to make between T.min and T.max
    # data = zip(*datapoints)
    # power_smooth = spline(data[0],data[1],xnew)
    # plt.plot(xnew,power_smooth,linestyle=l, label=legend,linewidth=2)
    plt.show()


synkit_results = []
for L in range(1,7):
    synkit_results_L = pd.read_csv('RESULTS/mynd-4GB-30min-L%s-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
    synkit_results_L.columns = ['domain','problem','runtime','status','errors']
    # results.index_col = 'config'
    synkit_results.append(synkit_results_L)
    
synkit_u_results = []
for L in range(1,7):
    synkit_u_results_L = pd.read_csv('RESULTS/mynd_sc-4GB-30min-L%s-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
    synkit_u_results_L.columns = ['domain','problem','runtime','status','errors']
    synkit_u_results.append(synkit_u_results_L)

def make_all_plots():
    syft_datapoints = []
    synkit_datapoints = []
    synkit_u_datapoints = []
    cumulated_synkit_datapoints = []
    cumulated_syft_synkit_datapoints = []
    
    synkit_compact_datapoints = []
    synkit_u_compact_datapoints = []
    synkit_decomposed_datapoints = []
    synkit_u_decomposed_datapoints = []
    
    cumulated_synkit_compact_datapoints = []
    cumulated_synkit_decomposed_datapoints = []
    cumulated_synkit_compact_decomposed_datapoints = []

    syft_results = pd.read_csv('syft_results.txt',  header=None, sep='\t')
    syft_results.columns = ['length','experiment','ltlf2fol','mona','syft','status']
    # results.index_col = 'config'

    
    for L in range(1,7):
        synkit_results_L = synkit_results[L-1]
        synkit_u_results_L = synkit_u_results[L-1]
        # synkit_results = pd.read_csv('RESULTS/mynd-L%s_realizability-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
        # synkit_results.columns = ['domain','problem','runtime','status','errors']
        # # results.index_col = 'config'

        # synkit_u_results = pd.read_csv('RESULTS/mynd-L%s_unrealizability-bigprobs-results.csv' % L, skiprows=1, header=None, sep=',')
        # synkit_u_results.columns = ['domain','problem','runtime','status','errors']

        
        syft_vec_L = syft_vec_solved_problems(syft_results,L)
        synkit_vec_L = synkit_vec_solved_problems(synkit_results_L)
        synkit_u_vec_L = synkit_vec_solved_problems(synkit_u_results_L)
        cumulated_synkit_vec_L = elementwise_or(synkit_vec_L,synkit_u_vec_L)
        cumulated_syft_synkit_vec_L = elementwise_or(elementwise_or(syft_vec_L, synkit_vec_L),synkit_u_vec_L)
        
        synkit_compact_vec_L = synkit_compact_vec_solved_problems(synkit_results_L)
        synkit_decomposed_vec_L = synkit_decomposed_vec_solved_problems(synkit_results_L)
        synkit_u_compact_vec_L = synkit_compact_vec_solved_problems(synkit_u_results_L)
        synkit_u_decomposed_vec_L = synkit_decomposed_vec_solved_problems(synkit_u_results_L)
        
        cumulated_synkit_compact_vec_L = elementwise_or(synkit_compact_vec_L,synkit_u_compact_vec_L)
        cumulated_synkit_decomposed_vec_L = elementwise_or(synkit_decomposed_vec_L,synkit_u_decomposed_vec_L)
        cumulated_synkit_composed_decomposed_vec_L = elementwise_or(cumulated_synkit_compact_vec_L,cumulated_synkit_decomposed_vec_L)
        
        syft_datapoint = (L,sum(syft_vec_L))
        synkit_datapoint = (L,sum(synkit_vec_L))
        synkit_u_datapoint = (L,sum(synkit_u_vec_L))
        cumulated_synkit_datapoint = (L,sum(cumulated_synkit_vec_L))
        cumulated_syft_synkit_datapoint = (L,sum(cumulated_syft_synkit_vec_L))

        synkit_compact_datapoint = (L,sum(synkit_compact_vec_L))
        synkit_u_compact_datapoint = (L,sum(synkit_u_compact_vec_L))
        synkit_decomposed_datapoint = (L,sum(synkit_decomposed_vec_L))
        synkit_u_decomposed_datapoint = (L,sum(synkit_u_decomposed_vec_L))

        syft_datapoints.append(syft_datapoint)
        synkit_datapoints.append(synkit_datapoint)
        synkit_u_datapoints.append(synkit_u_datapoint)
        cumulated_synkit_datapoints.append(cumulated_synkit_datapoint)
        cumulated_syft_synkit_datapoints.append(cumulated_syft_synkit_datapoint)

        synkit_compact_datapoints.append(synkit_compact_datapoint)
        synkit_u_compact_datapoints.append(synkit_u_compact_datapoint)
        synkit_decomposed_datapoints.append(synkit_decomposed_datapoint)
        synkit_u_decomposed_datapoints.append(synkit_u_decomposed_datapoint)
    
        cumulated_synkit_compact_datapoint = (L,sum(cumulated_synkit_compact_vec_L))
        cumulated_synkit_compact_datapoints.append(cumulated_synkit_compact_datapoint)

        cumulated_synkit_decomposed_datapoint = (L,sum(cumulated_synkit_decomposed_vec_L))
        cumulated_synkit_decomposed_datapoints.append(cumulated_synkit_decomposed_datapoint)
        
        cumulated_synkit_compact_decomposed_datapoint = (L,sum(cumulated_synkit_composed_decomposed_vec_L))
        cumulated_synkit_compact_decomposed_datapoints.append(cumulated_synkit_compact_decomposed_datapoint)
        
    fig, ax = plt.subplots(figsize=figsize)

    plot_one_line(ax,plt,syft_datapoints,'-.', 'o', "Syft")
    clr = "b" # blue
    plot_one_line(ax,plt,cumulated_syft_synkit_datapoints,'-.', '*', "Syft+SynKit", clr)
    
    # # plot_one_line(ax,plt,cumulated_synkit_datapoints,'-.', '^', "SynKit(real) + SynKit(unreal)")
    # plot_one_line(ax,plt,cumulated_synkit_datapoints,':', '^', "SynKit")
    # # plot_one_line(ax,plt,synkit_u_datapoints,':', '*', "SynKit(unreal)")
    # # plot_one_line(ax,plt,synkit_datapoints,':', 'v', "SynKit(real)")
    print plt.rcParams['axes.color_cycle']
    # ['#348ABD', '#7A68A6', '#A60628', '#467821', '#CF4457', '#188487', '#E24A33']
    clr =  u'#7A68A6'
    plot_one_line(ax,plt,cumulated_synkit_compact_decomposed_datapoints,'-.', '^', "SynKit (soft + hard)",clr)
    # plot_one_line(ax,plt,synkit_u_compact_datapoints,'--', 'o', "(Unreal, Soft)")
    # plot_one_line(ax,plt,synkit_u_decomposed_datapoints,':', '*', "(Unreal, Hard)")
    # plot_one_line(ax,plt,synkit_compact_datapoints,'-.', '>', "(Real, Soft)")
    # plot_one_line(ax,plt,synkit_decomposed_datapoints,'-.', '^', "(Real, Hard)")
    clr =  u'#A60628'
    plot_one_line(ax,plt,cumulated_synkit_compact_datapoints,'-.', '<', "SynKit (soft)",clr)
    clr =  u'#467821'
    plot_one_line(ax,plt,cumulated_synkit_decomposed_datapoints,'-.', 'v', "SynKit (hard)",clr)

    
    # plt.set_aspect('equal')
    # plt.plot([0, 1], [0, 1], transform=plt.transAxes)
    plt.xlabel('problem size')
    plt.ylabel('coverage')
    
    ax.set_xlim(0.5,6.5)

    legend = ax.legend(loc='lower left', numpoints=1, shadow=False)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        # label.set_fontsize('small')
        label.set_fontsize(14)
    
    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width
        
    fig.tight_layout()
    # plt.savefig('sum_sizes_vs_fml_len.pdf')
    plt.savefig('all_plots_realizability.pdf')
    plt.savefig('all_plots_realizability.ps')
    # print(df.loc[:,['automaton_sizes','subformulae_sizes']]).head()
    # print(pairs)


    import pylab
    # create a second figure for the legend
    figLegend = pylab.figure(figsize = (4,3))
    figLegend.suptitle('Legend (ordered by performance)', fontsize=20)
    # produce a legend for the objects in the other figure
    pylab.figlegend(*ax.get_legend_handles_labels(), loc = 'center', fontsize = 16)
    
    fig.tight_layout()
    # save the two figures to files
    figLegend.savefig("legend_realizability.pdf")


make_all_plots()